-- create image view
create view huni_image as
    select 
        image.id,
        image.title,
        image.caption,
        image.description,
        image.synopsis,
        record_type.record_type as type
    from
        image, record_type
    where
        image.record_type_id = record_type.id;

-- create bibliography view
create view huni_bibliography as
    select
        bibliography.id,
        bibliography.site_url,
        bibliography.description,
        bibliography.synopsis,
        bibliography.title,
        bibliography.subtitle,
        bibliography.chapter_title,
        bibliography.publication_date,
        bibliography.section,
        bibliography.volume,
        bibliography.issue,
        bibliography.town,
        bibliotype.description as bibliotype,
        country.description as country,
        record_type.record_type as type
    from
        bibliography, bibliotype, country, record_type
    where
        bibliography.bibliotype_id = bibliotype.id and
        bibliography.country_id = country.id and
        bibliography.record_type_id = record_type.id;

-- create company view
create view huni_company as
    select
        company.id,
        company.name,
        company.description,
        company.town,
        company.state_other,
        state.description as state,
        country.description as country
    from
        company, state, country
    where
        company.country_id = country.id and
        company.state_id = state.id; 

-- create cinema view
create view huni_cinema as
    select
        cinema_details.id,
        cinema_details.name,
        cinema_details.date_started,
        cinema_details.date_finished,
        cinema_details.address,
        cinema_details.suburb,
        cinema_details.postcode,
        state.description as state,
        country.description as country,
        cinema_details.date_built,
        cinema_details.date_demolished
    from
        cinema_details, state, country
    where
        cinema_details.state_id = state.id and
        cinema_details.country_id = country.id;


-- create person view
create view huni_person as
    select
        person.id,
        person.first_name,
        person.family_name,
        person.birth_year,
        person.death_year,
        person.description,
        country.description as country
    from
        person, country
    where
        person.country_id = country.id;


