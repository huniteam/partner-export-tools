#!/bin/bash

REPO="/home/mlarosa/huni/repo/afirc"
CACHE="/home/mlarosa/huni/afirc"
UPDATE_SCRIPT="/home/mlarosa/src/huni/src/repo-updater.py"

## run the repo updater  
if [ "$INFO" == "1" ] ; then
  $UPDATE_SCRIPT --cache $CACHE --repo $REPO --info
else
  $UPDATE_SCRIPT --cache $CACHE --repo $REPO
fi

## wipe the cache 
#find $CACHE -type f | xargs /bin/rm 

