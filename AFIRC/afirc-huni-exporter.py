#!/usr/bin/env python

from lxml import etree
import os
import os.path
import sys
import argparse
import ConfigParser
import logging 
import time

class counter:
    def __init__(self, total):
        self.last = 0
        self.total = total
        if total < 1000:
            self.logat = 100
        elif total < 100000:
            self.logat = 1000
        else:
            self.logat = 10000

    def update(self, current):
        if current > (self.last + self.logat):
            self.last += self.logat
            try:
                process_time = time.time() - self.last_time
                log.info("Processed %s of %s... (%s records in %1.2f seconds)" % (self.last, self.total, self.logat, float(process_time)))
            except:
                log.info("Processed %s of %s..." % (self.last, self.total))


class Flattenizer:
    def __init__(self, cfg):
        """Instantiating this object gets your config read and DB connection organised.

        You can access the variable names in the config as self.(name) inside the object.
        """

        # read in the config file
        self._ingest(cfg)

    def _ingest(self, cfg):
        """Read the config file - set var's as instance var's"""
        config = ConfigParser.SafeConfigParser()
        config_file = config.read(cfg)
        if not config_file:
            log.error("Does that config file exist? %s" % cfg)
            sys.exit()

        for section in config.sections():
            data = config.items(section)
            for param, value in data:
                setattr(self, param, value)
                log.debug("cfg: %s: %s" % (param, value))

#        self.process_tables = [table.strip() for table in self.process_tables.split(',') ]
#        log.debug("cfg: %s: %s" % ('process_tables', self.process_tables))
#        self.column_munge_list = [c.strip() for c in self.column_munging.split(',')]
#        log.debug("cfg: %s: %s" % ('column munge list', self.column_munge_list))

        # ensure the repo exists
        if not os.path.exists(self.repository):
            os.makedirs(self.repository)

    def flatten(self):
        """Iterate over the monolithic datafile producing individual records"""
        #datafile = etree.parse(self.datafile)

        for event, element in etree.iterparse(self.xml_datafile, 
            tag='{http://www.inmagic.com/webpublisher/query}Record'):

            total_records = element.getparent().get('setCount')
            break

        i = 0
        c = counter(int(total_records))
        for event, element in etree.iterparse(self.xml_datafile,
            tag='{http://www.inmagic.com/webpublisher/query}Record'):

            i += 1
            c.update(i)

            entity_id = element.xpath('inm:ID', namespaces= { 'inm': 'http://www.inmagic.com/webpublisher/query' })[0].text
            e = element.xpath('inm:Record-Type', namespaces= { 'inm': 'http://www.inmagic.com/webpublisher/query' })[0].text
            try:
                entity_name = e.replace(' ', '_')
            except AttributeError:
                #print etree.tostring(element, pretty_print=True)
                log.error('Skipping entity ID: %s' % entity_id)
                log.error("\n%s" % etree.tostring(element, pretty_print=True))
                #print sys.exc_info()
                continue

            datafile = "%s/%s%s-%s.xml" % (self.repository, self.identifier, entity_name, entity_id)
            log.debug("Writing %s" % datafile) 
            self.write(element, datafile)

            element.clear()

    def write(self, document, datafile):
        """Write document to datafile"""

        f = open(datafile, 'w')
        f.write(etree.tostring(document, pretty_print=self.pretty_print))
        f.close()
        t2 = time.time()


if __name__ == "__main__":

    # read and check the options
    parser = argparse.ArgumentParser(description='Flatten the AFIRC dataset')
    parser.add_argument('--config', required=True, dest='cfg',
        help="Configuration file.")

    parser.add_argument('--debug', dest='debug', action='store_true', help='Turn on full debugging (includes --info)')
    parser.add_argument('--info', dest='info', action='store_true', help='Turn on informational messages')
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    if args.info:
        logging.basicConfig(level=logging.INFO)

    if not (args.debug and args.info):
        # just give us error messages
        logging.basicConfig(level=logging.ERROR)

    log = logging.getLogger('AFIRC')
    f = Flattenizer(args.cfg)
    f.flatten()
