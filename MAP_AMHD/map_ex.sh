#!/bin/bash

REPO="/srv/huni/map"
CACHE="/home/mlarosa/huni/map"
CFG_FILE="/home/mlarosa/src/huni/config/map.conf"
EXPORT_SCRIPT="/home/mlarosa/src/huni/src/map-huni-exporter.py"
UPDATE_SCRIPT="/home/mlarosa/src/huni/src/repo-updater.py"

## run the export
if [ "$INFO" == "1" ] ; then
  $EXPORT_SCRIPT --config $CFG_FILE --info
else
  $EXPORT_SCRIPT --config $CFG_FILE  
fi

## run the repo updater 
if [ "$INFO" == "1" ] ; then
  $UPDATE_SCRIPT --cache $CACHE --repo $REPO --info 
else
  $UPDATE_SCRIPT --cache $CACHE --repo $REPO  
fi

## wipe the cache 
find $CACHE -type f | xargs /bin/rm 
