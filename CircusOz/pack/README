* Making your data source available to HuNI

A few steps are required - following is an overview of the procedure followed
by more detailed information as required.

1. install the dependencies for the scripts
2. Create the cache and repository directories
3. install the scripts
4. update the config file
5. run a test; check that all is well
6. configure to run nightly / weekly - as often as your data updates

1. Install the dependencies
---------------------------

** Debian / Ubuntu
==================
> aptitude update 
> aptitude install python-sqlaclchemy python-mysqldb python-lxml python-argparse

** RHEL / CentOS
================
You will need to enable the EPEL repository in order to get the python-argparse requirement.
see https://fedoraproject.org/wiki/EPEL

If you're on v6.x then the following commands should work:
> wget http://fedora.mirror.uber.com.au/epel/6/i386/epel-release-6-8.noarch.rpm 
> rpm -i epel-release-6-8.noarch.rpm
> yum install python-lxml python-sqlalchemy python-argparse MySQL-pythopython-argparse MySQL-python

The other dependencies should already be installed, but just in case they're not:
- bash and gzip

There is also a file called views.sql - ensure those views are created in your DB.


2. Create the cache and repository directories
----------------------------------------------

The export script requires a folder to write the data out to - this is the cache.

The repository updater script ensures the repository folder is kept up to date based
  on what is in the cache. The repository folder should be accessible from your main
  website via something like http://archive.circusoz.com/feed although the name
  can be different if you prefer.

3. Install the scripts
----------------------

In this folder there are 3 scripts that need to be installed somewhere on the system.
Any where you want is fine but it is suggested to follow standard practice so 
/usr/local/bin is the place you want.

  * circusoz-huni-export.py: is the script which will connect to your DB and export the data as XML files
  * repo-updater.py: will update the repository of datafiles from the cache
  * publish-resources.py: will create the resources file which tells HuNI what is available

The process is straightforward: given a cache directory, output the tables defined in the config file 
as XML files, then update the repository (a folder accessible via HTTP) from that content 
and publish a resources file which defines what is available.

4. Update the config file
--------------------------

The file "circusoz.conf" tells "circusoz-huni-export.py" what to do. Edit the file ensuring
you configure the "Database Connection Information" and set the "cache" appropriately; ie
somewhere the script can write to.

5. Run a test.
-------------- 

Assuming the scripts are installed in /usr/local/bin and the config file in /etc:
> /usr/local/bin/circusoz-huni-export.py --config /etc/circusoz.conf --info

Give the script some time to run - it will print informational messages out as it goes.

At the end, check the cache directory you defined to ensure you have datafiles in there.

Now you can test the repository updater:
> /usr/local/bin/repo-updater.py --info --cache {the cache directory you configured} --repo {the repo directory that is web accessible}

And finally the publisher:
> /usr/local/bin/publish-resources.py --repo {the repo directory} --info

6. Configure to run automatically daily or weekly.
--------------------------------------------------

A template cron job ("circusoz-export") is provided for installation into /etc/cron.daily or /etc/cron.weekly as you 
wish. Ensure you edit the variables at the top of the script to point to the actual 
location of all the bits.

