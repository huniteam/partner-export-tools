#!/usr/bin/env python

import sqlalchemy as sq
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import select
from lxml import etree
import os
import os.path
import filecmp
import shutil
import sys
import subprocess
import argparse
import ConfigParser
import logging 
import time

class counter:
    def __init__(self, total):
        self.last = 0
        self.total = total
        if total < 1000:
            self.logat = 100
        elif total < 100000:
            self.logat = 1000
        else: 
            self.logat = 10000

    def update(self, current):
        if current > (self.last + self.logat):
            self.last += self.logat
            try:
                process_time = time.time() - self.last_time
                log.info("Processed %s of %s... (%s records in %1.2f seconds)" % (self.last, self.total, self.logat, float(process_time)))
            except:
                log.info("Processed %s of %s..." % (self.last, self.total))

class Flattenizer:
    def __init__(self, cfg):
        """Instantiating this object gets your config read and DB connection organised.

        You can access the variable names in the config as self.(name) inside the object.
        """

        # read in the config file
        self._ingest(cfg)

        # plug in to the db
        # http://docs.sqlalchemy.org/en/latest/core/schema.html#reflecting-database-objects
        try:
            engine = create_engine('%s://%s:%s@%s/%s' % (self.dbtype, self.dbuser, self.dbpass, self.dbhost, self.dbname),
                pool_size=50, pool_timeout=1, pool_recycle=1 )
        except:
            print "Ooops - couldn't connect to db. Check your config"
            sys.exit()
        self.conn = engine.connect()
        self.Session = sessionmaker(bind=engine)
        self.meta = sq.schema.MetaData(bind=engine)
        self.meta.reflect()
        log.debug('engine: %s' % engine)

    def _ingest(self, cfg):
        """Read the config file - set var's as instance var's"""
        config = ConfigParser.SafeConfigParser()
        config_file = config.read(cfg)
        if not config_file:
            log.error("Does that config file exist? %s" % cfg)
            sys.exit()

        for section in config.sections():
            data = config.items(section)
            for param, value in data:
                setattr(self, param, value)
                log.debug("cfg: %s: %s" % (param, value))

        self.process_tables = [table.strip() for table in self.process_tables.split(',') ]
        log.debug("cfg: %s: %s" % ('process_tables', self.process_tables))
        self.column_munge_list = [c.strip() for c in self.column_munging.split(',')]
        log.debug("cfg: %s: %s" % ('column munge list', self.column_munge_list))

        # ensure the repo exists
        if not os.path.exists(self.cache):
            os.makedirs(self.cache)

        # ensure the cache exists - remove it first if it does
        #  (crude way to wipe it but whatever)
        #  this is where we write our data 
        if os.path.exists(self.cache):
            log.info("Wiping the cache")
            shutil.rmtree(self.cache)
        os.mkdir(self.cache)

    def table(self, t):
        """Return a SQLAlchemy representation of the database table"""
        return sq.schema.Table(str(t), self.meta, autoload=True)

    def columns(self, table):
        """Return a list of the table columns"""
        return [str(col).split('.')[1] for col in table.columns]

    def flatten(self, table, one):
        """Iterate over table and process the rows into XML files
        @params:
        table: the database table to process

        one: process one row of the table only - no guarantees which
            row you get... sqlalchemy makes that decision.
        """
        log.info("Writing: %s" % table)
        
        # blank the logging counter for each table
        self.logat = 0
        self.last = 0

        # if the table is in the form table.{something}
        #  then that's the admin trying to tell us which column to use
        #  as the entity id.
        id_field = None
        try:
            (table, id_field) = table.split('.')
        except:
            pass

        table = self.table(table)

        s = select([table])
        data = self.conn.execute(s)
        columns = self.columns(table)
        if one:
            row = data.fetchone()
            self.process(row, table, columns, id_field)
        else:
            i = 0
            c = counter(data.rowcount)
            for row in data:
                i += 1
                c.update(i)
                self.process(row, table, columns, id_field)

    def datafile(self, entity_name, entity_id):
        entity_cache = "%s/%s" % (self.cache, entity_name)
        if not os.path.exists(entity_cache):
            os.mkdir(entity_cache)

        try:
            entity_cache = "%s/%s" % (entity_cache, (int(entity_id) / 1000))
        except TypeError:
            # NoneType
            entity_cache = "%s/%s" % (entity_cache, "None")

        if not os.path.exists(entity_cache):
            os.mkdir(entity_cache)

        return "%s/%s%s-%s.xml" % (entity_cache, self.identifier, entity_name, entity_id)

    def process(self, row, table, columns, id_field):
        """Construct an XML file of the data contained in the row.

        Not to be used directly: go read flatten()

        @params:
        row: a table row to be processed.
        table: the table in question.
        columns: a list of the columns.
        """
        if id_field is not None:
            pkey = id_field
        else:
            pkeys = [str(k).split('.')[1] for k in table.primary_key]
            pkey = pkeys[0]

        if row is not None:
            datafile = self.datafile(table, row[pkey])
            root = etree.Element(str(table), xmlns=self.namespace)
            for c in columns:
                column_original_name = c
                column_output_name = c
                for thing in self.column_munge_list:
                    column_output_name = column_output_name.replace(thing, '')
                self.add(root, column_output_name, row[column_original_name])

        # write the document
        self.write(root, datafile)

    def add(self, document, element, data):
        """Add an XML element to an XML document
        
        @params:
        document: the document to add the element to
        element: the element to add to the document
        data: the data for the element
        """
        e = etree.Element(element)
        try:
            e.text = unicode(str(data), encoding=self.data_encoding)
            document.append(e)
        except UnicodeDecodeError:
            log.error("Unicode Data Error - element: %s, data: %s" % (element, data))
        except ValueError:
            log.error("Data Error %s" % data)

    def write(self, document, datafile):
        """Write document to datafile"""

        f = open(datafile, 'w')
        f.write(etree.tostring(document, pretty_print=self.pretty_print))
        f.close()
        t2 = time.time()


if __name__ == "__main__":

    # read and check the options
    parser = argparse.ArgumentParser(description='Flatten a database')
    parser.add_argument('--one', action="store_true", dest="one",
        help="Fetch only one row of each table - useful for debugging")
    parser.add_argument('--config', required=True, dest='cfg',
        help="Configuration file.")

    parser.add_argument('--debug', dest='debug', action='store_true', help='Turn on full debugging (includes --info)')
    parser.add_argument('--info', dest='info', action='store_true', help='Turn on informational messages')
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    if args.info:
        logging.basicConfig(level=logging.INFO)

    if not (args.debug and args.info):
        # just give us error messages
        logging.basicConfig(level=logging.ERROR)

    log = logging.getLogger('AusStage')
    f = Flattenizer(args.cfg)
    for t in f.process_tables:
        f.flatten(t, args.one)



