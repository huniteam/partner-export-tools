-- create huni_events view
drop view if exists huni_events;
create view huni_events as
  select
    events.eventid,
    events.event_name,
    events.umbrella,
    events.entered_by,
    events.dddate_entered,
    events.mmdate_entered,
    events.yyyydate_entered,
    events.snddate_entered,
    events.dddate_updated,
    events.mmdate_updated,
    events.yyyydate_updated,
    events.snddate_updated,
    events.description,
    events.data_source1,
    events.data_source2,
    events.data_source3,
    events.data_source_description1,
    events.data_source_description2,
    events.data_source_description3,
    events.part_of_a_tour,
    events.world_premier,
    events.ddfirst_date,
    events.mmfirst_date,
    events.yyyyfirst_date,
    events.sndfirst_date,
    events.ddlast_date,
    events.mmlast_date,
    events.yyyylast_date,
    events.sndlast_date,
    events.collection1,
    events.collection2,
    events.collection3,
    events.status,
    events.venueid,
    venue.venue_name,
    events.primary_genre,
    prigenreclass.genreclass,
    events.content_indicator,
    events.further_information,
    events.description_source,
    events.ddopening_night,
    events.mmopening_night,
    events.yyyyopening_night,
    events.sndopening_night,
    events.estimated_dates,
    events.entered_by_user,
    events.interesting_perf_history,
    events.entered_date,
    events.updated_date,
    events.first_date,
    events.last_date,
    events.opening_night_date,
    events.updated_by_user,
    events.review
  from
    events
  left join prigenreclass on events.primary_genre = prigenreclass.genreclassid
  left join venue on events.venueid = venue.venueid;

-- create huni_contributors view
drop view if exists huni_contributors;
create view huni_contributors as
  select
    contributor.contributorid,
    contributor.last_name,
    contributor.first_name,
    contributor.gender as genderid,
    gendermenu.gender as gender,
    if (ISNULL(contributor.date_of_death) and ISNULL(contributor.yyyydate_of_death), NULL, contributor.dddate_of_birth) as dddate_of_birth,
    if (ISNULL(contributor.date_of_death) and ISNULL(contributor.yyyydate_of_death), NULL, contributor.mmdate_of_birth) as mmdate_of_birth,
    if (ISNULL(contributor.date_of_death) and ISNULL(contributor.yyyydate_of_death), NULL, contributor.yyyydate_of_birth) as yyyydate_of_birth,
    contributor.snddate_of_birth,
    contributor.nationality,
    contributor.other_names,
    contributor.address,
    contributor.suburb,
    contributor.state as stateid,
    states.state as state,
    contributor.postcode,
    contributor.email,
    contributor.notes,
    contributor.contributorcountry,
    if (ISNULL(contributor.date_of_death) and ISNULL(contributor.yyyydate_of_death), NULL, contributor.date_of_birth) as date_of_birth,
    contributor.countryid,
    country.countryname,
    contributor.dddate_of_death,
    contributor.mmdate_of_death,
    contributor.yyyydate_of_death,
    contributor.date_of_death,
    contributor.prefix,
    contributor.middle_name,
    contributor.suffix,
    contributor.place_of_birth,
    contributor.place_of_death,
    contributor.display_name,
    contributor.nla,
    contributor.updated_by_user,
    contributor.entered_by_user,
    contributor.entered_date,
    contributor.updated_date
  from
    contributor
  left join gendermenu on contributor.gender = gendermenu.genderid
  left join states on contributor.state = states.stateid
  left join country on contributor.countryid = country.countryid;

-- create huni_item view
drop view if exists huni_item;
create view huni_item as
  select
    item.itemid,
    item.catalogueid,
    item.institutionid,
    organisation.name as organisation_name,
    item.item_description,
    item.item_condition_id,
    `condition`.`condition` as item_condition,
    item.detail_comments,
    item.donated_purchased,
    item.aquired_from,
    item.storage,
    item.provenance,
    item.significance,
    item.comments,
    item.entered_by_user,
    item.entered_date,
    item.updated_date,
    item.item_url,
    item.description_abstract,
    item.format_extent,
    item.format_medium,
    item.format_mimetype,
    item.format,
    item.ident_isbn,
    item.ident_ismn,
    item.ident_issn,
    item.ident_sici,
    item.publisher,
    item.rights_access_rights,
    item.rights,
    item.rights_holder,
    item.title,
    item.title_alternative,
    item.dc_creator,
    item.ddcreated_date,
    item.mmcreated_date,
    item.yyyycreated_date,
    item.created_date,
    item.ddcopyright_date,
    item.mmcopyright_date,
    item.yyyycopyright_date,
    item.copyright_date,
    item.ddissued_date,
    item.mmissued_date,
    item.yyyyissued_date,
    item.issued_date,
    item.ddaccessioned_date,
    item.mmaccessioned_date,
    item.yyyyaccessioned_date,
    item.accessioned_date,
    item.item_type_lov_id,
    item_type_lov.description as item_type,
    item.citation,
    item.modified_by_user,
    item.modified_date,
    item.sourceid,
    item.item_sub_type_lov_id,
    item_sub_type_lov.description as item_sub_type,
    item.date_notes,
    item.publisher_location,
    item.volume,
    item.issue,
    item.page,
    item.language_lov_id,
    language_lov.description as language,
    item.ddterminated_date,
    item.mmterminated_date,
    item.yyyyterminated_date,
    item.terminated_date
  from
    item
  left join organisation on item.institutionid = organisation.organisationid
  left join `condition` on item.item_condition_id = `condition`.condition_id
  left join lookup_codes as language_lov on item.language_lov_id = language_lov.code_lov_id
  left join lookup_codes as item_type_lov on item.item_type_lov_id = item_type_lov.code_lov_id
  left join lookup_codes as item_sub_type_lov on item.item_sub_type_lov_id = item_sub_type_lov.code_lov_id;

-- create huni_organisation view
drop view if exists huni_organisation;
create view huni_organisation as
  select
    organisation.organisationid,
    organisation.name,
    organisation.other_names1,
    organisation.other_names2,
    organisation.other_names3,
    organisation.address,
    organisation.suburb,
    organisation.state as stateid,
    states.state as state,
    organisation.postcode,
    organisation.contact,
    organisation.phone1,
    organisation.phone2,
    organisation.phone3,
    organisation.fax,
    organisation.email,
    organisation.web_links,
    organisation.notes,
    organisation.organisationcountry,
    organisation.countryid,
    country.countryname,
    organisation.organisation_type_id,
    organisation_type.type,
    organisation.ddfirst_date,
    organisation.mmfirst_date,
    organisation.yyyyfirst_date,
    organisation.ddlast_date,
    organisation.mmlast_date,
    organisation.yyyylast_date,
    organisation.place_of_origin,
    organisation.place_of_demise,
    organisation.nla,
    organisation.updated_by_user,
    organisation.entered_by_user,
    organisation.entered_date,
    organisation.updated_date
  from
      organisation
  left join states on organisation.state = states.stateid
  left join country on organisation.countryid = country.countryid
  left join organisation_type on organisation.organisation_type_id = organisation_type.organisation_type_id;

-- create huni_venue view
drop view if exists huni_venue;
create view huni_venue as
  select
    venue.venueid,
    venue.venue_name,
    venue.street,
    venue.suburb,
    venue.state as stateid,
    states.state,
    venue.capacity,
    venue.contact,
    venue.phone,
    venue.fax,
    venue.email,
    venue.web_links,
    venue.notes,
    venue.countryid,
    country.countryname,
    venue.longitude,
    venue.latitude,
    venue.regional_or_metro,
    venue.postcode,
    venue.ddfirst_date,
    venue.mmfirst_date,
    venue.yyyyfirst_date,
    venue.ddlast_date,
    venue.mmlast_date,
    venue.yyyylast_date,
    venue.radius,
    venue.elevation,
    venue.updated_date,
    venue.entered_date,
    venue.entered_by_user,
    venue.updated_by_user,
    venue.other_names1,
    venue.other_names2,
    venue.other_names3
  from
      venue
  left join states on venue.state = states.stateid
  left join country on venue.countryid = country.countryid;

-- output record counts
select "==========================" as '';
select "HuNI views by record count" as '';
select "==========================" as '';

select       'huni_events'       as huni_view, count(*) as records from huni_events
UNION select 'huni_contributors' as huni_view, count(*) as records from huni_contributors
UNION select 'huni_item'         as huni_view, count(*) as records from huni_item
UNION select 'huni_organisation' as huni_view, count(*) as records from huni_organisation
UNION select 'huni_venue'        as huni_view, count(*) as records from huni_venue
UNION select 'TOTAL'             as huni_view, count(*) as records from (
  select       'huni_events', eventid              from huni_events
  UNION select 'huni_contributors', contributorid  from huni_contributors
  UNION select 'huni_item', itemid                 from huni_item
  UNION select 'huni_organisation', organisationid from huni_organisation
  UNION select 'huni_venue', venueid               from huni_venue
) as huni_all_views;
