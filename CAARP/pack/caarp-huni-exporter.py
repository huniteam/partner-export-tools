#!/usr/bin/env python

import sqlalchemy as sq
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import select
from lxml import etree
import os
import os.path
import filecmp
import shutil
import sys
import subprocess
import argparse
import ConfigParser
import logging 
import time


class Flattenizer:
    def __init__(self, cfg):
        """Instantiating this object gets your config read and DB connection organised.

        You can access the variable names in the config as self.(name) inside the object.
        """

        # read in the config file
        self._ingest(cfg)

        # plug in to the db
        # http://docs.sqlalchemy.org/en/latest/core/schema.html#reflecting-database-objects
        try:
            engine = create_engine('%s://%s:%s@%s/%s' % (self.dbtype, self.dbuser, self.dbpass, self.dbhost, self.dbname),
                pool_size=50, pool_timeout=1, pool_recycle=1 )
        except:
            print "Ooops - couldn't connect to db. Check your config"
            sys.exit()
        self.conn = engine.connect()
        self.Session = sessionmaker(bind=engine)
        self.meta = sq.schema.MetaData(bind=engine)
        self.meta.reflect()
        log.debug('engine: %s' % engine)

    def _ingest(self, cfg):
        """Read the config file - set var's as instance var's"""
        config = ConfigParser.SafeConfigParser()
        config_file = config.read(cfg)
        if not config_file:
            log.error("Does that config file exist? %s" % cfg)
            sys.exit()

        for section in config.sections():
            data = config.items(section)
            for param, value in data:
                setattr(self, param, value)
                log.debug("cfg: %s: %s" % (param, value))

        self.process_tables = [table.strip() for table in self.process_tables.split(',') ]
        log.debug("cfg: %s: %s" % ('process_tables', self.process_tables))
        self.column_munge_list = [c.strip() for c in self.column_munging.split(',')]
        log.debug("cfg: %s: %s" % ('column munge list', self.column_munge_list))

        # ensure the repo exists
        if not os.path.exists(self.repository):
            os.makedirs(self.repository)

        # ensure the cache exists
        #  this is where we write our data - git then tells
        #  us what to change in the real repo
        if not os.path.exists(self.cache):
            os.mkdir(self.cache)

    def table(self, t):
        """Return a SQLAlchemy representation of the database table"""
        return sq.schema.Table(str(t), self.meta, autoload=True)

    def columns(self, table):
        """Return a list of the table columns"""
        return [str(col).split('.')[1] for col in table.columns]

    def flatten(self, table, one):
        """Iterate over table and process the rows into XML files
        @params:
        table: the database table to process

        one: process one row of the table only - no guarantees which
            row you get... sqlalchemy makes that decision.
        """
        log.info("Writing: %s" % table)
        
        # blank the logging counter for each table
        self.logat = 0
        self.last = 0

        s = select([table])
        data = self.conn.execute(s)
        columns = self.columns(table)
        if one:
            row = data.fetchone()
            self.process(row, table, columns)
        else:
            i = 0
            for row in data:
                i += 1
                self.counter(data.rowcount, i)
                self.process(row, table, columns)

    def counter(self, total, current):
        if total < 1000:
            self.logat = 100
        elif total < 100000:
            self.logat = 1000
        else:
            self.logat = 10000

        #print total, current, self.logat
        if current > (self.last + self.logat):
            self.last += self.logat
            try:
                process_time = time.time() - self.last_time
                log.info("Processed %s of %s... (%s records in %1.2f seconds)" % (self.last, total, self.logat, float(process_time)))
            except:
                log.info("Processed %s of %s..." % (self.last, total))
                
            self.last_time = time.time()

    def datafile(self, entity_name, entity_id):
        entity_cache = "%s/%s" % (self.cache, entity_name)
        if not os.path.exists(entity_cache):
            os.mkdir(entity_cache)

        try:
            entity_cache = "%s/%s" % (entity_cache, (int(entity_id) / 1000))
        except TypeError:
            # NoneType
            entity_cache = "%s/%s" % (entity_cache, "None")

        if not os.path.exists(entity_cache):
            os.mkdir(entity_cache)

        return "%s/%s%s-%s.xml" % (entity_cache, self.identifier, entity_name, entity_id)

    def column_cleaner(self, column):
        column_output_name = column
        for thing in self.column_munge_list:
            column_output_name = column_output_name.replace(thing, '')
        return column_output_name

    def process(self, row, table, columns):
        """Construct an XML file of the data contained in the row.

        Not to be used directly: go read flatten()

        @params:
        row: a table row to be processed.
        table: the table in question.
        columns: a list of the columns.
        """
        pkeys = [str(k).split('.')[1] for k in table.primary_key]
        if row is not None:
            datafile = self.datafile(table, row[pkeys[0]])
            root = etree.Element(str(table), xmlns=self.namespace)
            for c in columns:
                column_original_name = c
                column_output_name = self.column_cleaner(c)
                self.add(root, column_output_name, row[column_original_name])

        # write the document
        self.write(root, datafile)

    def add(self, document, element, data):
        """Add an XML element to an XML document
        
        @params:
        document: the document to add the element to
        element: the element to add to the document
        data: the data for the element
        """
        e = etree.Element(element)
        try:
            e.text = unicode(str(data), encoding=self.data_encoding)
            document.append(e)
        except UnicodeDecodeError:
            log.error("Unicode Data Error - element: %s, data: %s" % (element, data))
        except ValueError:
            log.error("Data Error %s" % data)

    def write(self, document, datafile):
        """Write document to datafile"""

        f = open(datafile, 'w')
        f.write(etree.tostring(document, pretty_print=self.pretty_print))
        f.close()
        t2 = time.time()


    def linkify(self, table, maps):
        """Handle a links table

        @params:
        table: the table to process
        one: if True, process one row only
        maps: list of lists

            maps = 
                [ 'film.id', 'screenings.film_id', 'screenings', 'screenings.film_id' ]
            ]
            
            Find the file called 'film-' with the id of 'screenings.film_id' and add to it
            an element of 'screenings' with the content of 'screenings.film_id'
        """
        log.info("Processing links from '%s'" % str(table))

        # for each of the maps
        for what in maps:
            log.debug("Processing: %s" % what)

            # blank the logging counter 
            self.logat = 0
            self.last = 0

            # get the details (name and relevant id field) of the
            #  entities we wish to augment
            entity_name = what[0].split('.')[0]
            src_table_name = self.table(entity_name)
            src_table_id_field = what[0].split('.')[1]

            # also get the table / field where the augmented data comes from
            tgt_table_name = self.table(what[1].split('.')[0])
            tgt_table_id_field = what[1].split('.')[1]

            # and store the data defining the element we're going to add
            element_name = what[2]
            element_data_field = what[3].split('.')[1]

            session = self.Session()
            entities = session.query(src_table_name.c[src_table_id_field]).all()
            total_entities = len(entities)
            session.close()

            # iterate over the entities to be updated
            i = 0
            for entity in entities:
                i += 1
                self.counter(total_entities, i)
                entity_id = getattr(entity, src_table_id_field)

                # it is highly likely that we find empty link columns - the implication
                #  being that we're apparently looking for non-existent files.. skip those
                if entity_name is None or entity_id is None:
                    continue
                datafile = self.datafile(entity_name, entity_id)
                try:
                    document = etree.parse(datafile).getroot()
                except IOError:
                    log.error("Couldn't find %s" % datafile)
                except etree.XMLSyntaxError:
                    pass
                    #log.error('Empty XML document - can it really be so ... ?')
                    #log.error("The offending datafile is: %s " % datafile)

                # for each entity we want to update, auery the target table for the related
                #  and add it to the entity we're handling
                session = self.Session()
                results = session.query(tgt_table_name.c[element_data_field]).filter(tgt_table_name.c[tgt_table_id_field] == entity_id).all()
                session.close()

                element = str(table)
                [ self.add(document, element, getattr(result, element_data_field)) for result in results ]

                # write the file
                self.write(document, datafile)

    def ingest_table(self, src_table, src_id_field, tgt_table, tgt_id_field, src_entity_id=None):
        log.info("Ingesting '%s' data in to '%s'" % (tgt_table, src_table))

        if src_entity_id == None:
            src_entity_id = src_id_field

        # blank the logging counter for each table
        self.logat = 0
        self.last = 0

        # connect to the film table and get the rows
        s = select([src_table])
        data = self.conn.execute(s)

        i = 0
        # iterate over the rows of the film table
        for row in data:
            i += 1
            self.counter(data.rowcount, i)

            datafile = self.datafile(src_table, row[src_entity_id])
            try:
                document = etree.parse(datafile).getroot()
            except IOError:
                log.error("Couldn't find %s" % datafile)
                log.error("can't add '%s' to '%s-%s'" % (tgt_table, src_table, row[src_id_field]))
                continue

            session = self.Session()
            records = session.query(tgt_table).filter(tgt_table.c[tgt_id_field] == row[src_id_field])
            #print records.count()
            for record in records:
                root = etree.Element(str(tgt_table))
                for c in self.columns(tgt_table):
                    column_output_name = self.column_cleaner(c)
                    self.add(root, column_output_name, getattr(record, c))

                # and add the element
                document.append(root)

            # write the file
            self.write(document, datafile)
            session.close()


if __name__ == "__main__":

    # read and check the options
    parser = argparse.ArgumentParser(description='Flatten a database')
    parser.add_argument('--one', action="store_true", dest="one",
        help="Fetch only one row of each table - useful for debugging")
    parser.add_argument('--config', required=True, dest='cfg',
        help="Configuration file.")

    parser.add_argument('--debug', dest='debug', action='store_true', help='Turn on full debugging (includes --info)')
    parser.add_argument('--info', dest='info', action='store_true', help='Turn on informational messages')
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    if args.info:
        logging.basicConfig(level=logging.INFO)

    if not (args.debug and args.info):
        # just give us error messages
        logging.basicConfig(level=logging.ERROR)

    log = logging.getLogger('CAARP')
    f = Flattenizer(args.cfg)
    for t in f.process_tables:
        if t == 'film_title':
            f.ingest_table(f.table('film'), 'id', f.table('film_title'), 'film_id')

        elif t == 'venue_operation_dates':
            f.ingest_table(f.table('venue'), 'id', f.table('venue_operation_dates'), 'venue_id' )

        elif t == 'venue_attributes':
            f.ingest_table(f.table('venue'), 'id', f.table('venue_attributes'), 'venue_id' )

        else:
            f.flatten(f.table(t), args.one)



